package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Student struct {
	Name string `json:"name;omitempty"`
	Age  string `json:"age;omitempty"`
}

var client *mongo.Client
var ctx context.Context

func init() {

	fmt.Println("Starting application...")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, _ = mongo.Connect(ctx, options.Client().ApplyURI("mongodb://db:27017"))
}
func main() {
	testCollection := client.Database("test").Collection("student")
	result, err := testCollection.InsertOne(ctx, bson.M{"name": "test", "age": 12})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(result)
	r := mux.NewRouter()
	r.HandleFunc("/student", insertStudent).Methods("POST")
	r.HandleFunc("/student", getStudents).Methods("GET")
	r.HandleFunc("/hello", helloHandle).Methods("GET")
	http.ListenAndServe(":9999", r)
	defer client.Disconnect(ctx)

}
func helloHandle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("hello api called...")
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode("hello world")
}
func insertStudent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	reqBody, _ := ioutil.ReadAll(r.Body)
	var student Student
	json.Unmarshal(reqBody, &student)
	testCollection := client.Database("test").Collection("student")
	result, err := testCollection.InsertOne(ctx, bson.M{"name": student.Name, "age": student.Age})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(result)
	json.NewEncoder(w).Encode(student)
}
func getStudents(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	userData := []Student{}
	testDB := client.Database("test")
	fmt.Println(testDB)
	testCollection := testDB.Collection("student")
	fmt.Println(testCollection)
	cursor, err := testCollection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var student Student
		cursor.Decode(&student)
		userData = append(userData, student)
	}
	fmt.Println(userData)
	json.NewEncoder(w).Encode(userData)
}
